COMMON=registry.gitlab.com/cmd.run/common

build/curl:
	@cd curl && DOCKER_BUILDKIT=1 docker build -t ${COMMON}:curl .

build/wget:
	@cd wget && DOCKER_BUILDKIT=1 docker build -t ${COMMON}:wget .

build/git:
	@cd git && DOCKER_BUILDKIT=1 docker build -t ${COMMON}:git .

build/jq:
	@cd jq && DOCKER_BUILDKIT=1 docker build -t ${COMMON}:jq .

test/curl:
	@docker run --rm -it -v ${PWD}/:/workspace ${COMMON}:curl curl --help

test/wget:
	@docker run --rm -it -v ${PWD}/:/workspace ${COMMON}:wget wget --help

test/git:
	@docker run --rm -it -v ${PWD}/:/workspace ${COMMON}:git git --help

test/jq:
	@docker run --rm -it -v ${PWD}/:/workspace ${COMMON}:jq jq --help
