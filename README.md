<div align="center">

# cmd.run/common

Containerization for Common Tools 🚀⚡🔥<br>

_Suggestions are always welcome!_

</div>

## Description

This repository serves as a unified place for Dockerfiles and images for common tools used in development.

## Prerequisites

Download and install:

- [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04) 

## What images are included?

- [x] [curl](https://curl.se/)
- [x] [wget](https://www.gnu.org/software/wget/)
- [x] [git](https://git-scm.com/)
- [x] [jq](https://stedolan.github.io/jq/)

## Potential images

- [ ] [wikit](https://github.com/KorySchneider/wikit)
- [ ] [mdbook](https://github.com/rust-lang/mdBook)
- [ ] [yt-dlc](https://github.com/blackjack4494/yt-dlc)
- [ ] [jekyll](https://jekyllrb.com/)
- [ ] [pandoc](https://pandoc.org/)
- [ ] [nodejs](https://nodejs.org/) with alt managers
- [ ] [whaler](https://github.com/P3GLEG/Whaler)

## What's included in this repo?

- A minimal and clean set of _Dockerfiles_ that defines the installation of above mentioned packages
- A _makefile_ that simplifies building and testing if you are utilizing linux

## Getting Started

**To use a specific image directly**

1. Pull the image from GitLab:
```sh
docker pull registry.gitlab.com/cmd.run/<TOOL>:latest
```

**To build it from scratch**

1. Clone the repository and switch directories:

```sh
git clone https://gitlab.com/cmd.run/common.git
cd common
```

2. Start the build:
   
```sh
make build/<TOOL>
``` 

## License

This project is licensed under the MIT License.

```
MIT License

Copyright (c) 2022 machinelearning-one

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
